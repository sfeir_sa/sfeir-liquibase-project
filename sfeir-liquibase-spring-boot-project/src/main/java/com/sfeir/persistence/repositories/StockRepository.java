package com.sfeir.persistence.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sfeir.persistence.entities.Stock;

/**
 * Stock repository
 * 
 * @author Kheireddine BOURAHLI
 * @version 0.0.1
 *
 */
public interface StockRepository extends CrudRepository<Stock, Integer> {

}
