package com.sfeir.persistence.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sfeir.persistence.entities.DetailStock;

/**
 * Stock detail repository
 * 
 * @author Kheireddine BOURAHLI
 * @version 0.0.1
 *
 */
public interface StockDetailRepository extends CrudRepository<DetailStock, Integer> {

}
