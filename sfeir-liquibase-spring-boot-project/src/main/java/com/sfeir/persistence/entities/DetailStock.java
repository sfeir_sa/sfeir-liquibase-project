package com.sfeir.persistence.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entité détail du stock
 * 
 * @author Kheireddine BOURAHLI
 * @version 0.0.1
 */
@Entity
@Table(name = "DETAIL_STOCK")
public class DetailStock {

	/**
	 * identifiant de Detail stock
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DETAIL_STOCK_ID", unique = true, nullable = false)
	private Integer stockId;

	/**
	 * nom de la société
	 */
	@Column(name = "COMP_NAME", nullable = false, length = 100)
	private String compName;

	/**
	 * Description de la société
	 */
	@Column(name = "COMP_DESC", nullable = false)
	private String compDesc;

	/**
	 * Remarque
	 */
	@Column(name = "REMARK", nullable = false)
	private String remark;

	/**
	 * la date de création
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "LISTED_DATE", nullable = false, length = 10)
	private Date listedDate;

	/**
	 * les informations sur le stock
	 */
	@OneToOne
	@JoinColumn(name = "stock")
	private Stock stock;

	protected DetailStock() {
		// for JPA
	}

	public Integer getStockId() {
		return stockId;
	}

	public String getCompName() {
		return compName;
	}

	public String getCompDesc() {
		return compDesc;
	}

	public String getRemark() {
		return remark;
	}

	public Stock getStock() {
		return stock;
	}

	public Date getListedDate() {
		return listedDate;
	}
}
