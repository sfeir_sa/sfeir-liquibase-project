package com.sfeir.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entité Stock
 * 
 * @author Kheireddine BOURAHLI
 * @version 0.0.1
 *
 */
@Entity
@Table(name = "STOCK")
public class Stock implements Serializable {

	/**
	 * identifiant du stock
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "STOCK_ID", unique = true, nullable = false)
	private Integer stockId;

	/**
	 * le code du stock
	 */
	@Column(name = "STOCK_CODE", unique = true, nullable = false, length = 10)
	private String stockCode;

	/**
	 * le nom du stock
	 */
	@Column(name = "STOCK_NAME", unique = true, nullable = false, length = 20)
	private String stockName;

	public Integer getStockId() {
		return stockId;
	}

	public String getStockCode() {
		return stockCode;
	}

	public String getStockName() {
		return stockName;
	}
}