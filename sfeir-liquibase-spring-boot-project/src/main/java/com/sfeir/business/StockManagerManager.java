package com.sfeir.business;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sfeir.persistence.entities.DetailStock;
import com.sfeir.persistence.repositories.StockDetailRepository;

/**
 * Stock detail repository
 * 
 * @author Kheireddine BOURAHLI
 * @version 0.0.1
 *
 */
@Component
public class StockManagerManager {

	private StockDetailRepository stockDetailRepository;

	@Autowired
	public StockManagerManager(StockDetailRepository stockDetailRepository) {
		this.stockDetailRepository = stockDetailRepository;
	}

	/**
	 * Recuperer les informations sur les stocks
	 * 
	 * @return {@link DetailStock}
	 */
	public Stream<DetailStock> getAllStockDetails() {
		return StreamSupport.stream(stockDetailRepository.findAll().spliterator(), true);
	}
}
