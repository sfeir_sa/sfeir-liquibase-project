package com.sfeir.business;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sfeir.persistence.entities.Stock;
import com.sfeir.persistence.repositories.StockRepository;

/**
 * Stock service
 * 
 * @author Kheireddine BOURAHLI
 * @version 0.0.1
 *
 */
@Component
public class StockManager {

	/**
	 * Repository stock
	 */
	private StockRepository stockRepository;

	@Autowired
	public StockManager(StockRepository stockRepository) {
		this.stockRepository = stockRepository;
	}

	/**
	 * Récuperer les stocks qui se trouvent dans la base de données
	 * 
	 * @return liste de stock {@link Stock}
	 */
	public Stream<Stock> getAllStocks() {
		return StreamSupport.stream(stockRepository.findAll().spliterator(), true);
	}
}
