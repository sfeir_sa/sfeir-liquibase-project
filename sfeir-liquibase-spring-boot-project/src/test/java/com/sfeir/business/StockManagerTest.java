package com.sfeir.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sfeir.persistence.entities.Stock;

/**
 * Test la classe Stock manager
 * 
 * @author kheireddine BOURAHLI
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StockManagerTest {

	@Autowired
	private StockManager stockManager;

	@Test
	public void getAllStocksReturnDataFromDatabase() throws Exception {
		List<Stock> stocks = stockManager.getAllStocks().collect(Collectors.toList());
		assertFalse(stocks.isEmpty());
		assertEquals(5, stocks.size());
	}
}