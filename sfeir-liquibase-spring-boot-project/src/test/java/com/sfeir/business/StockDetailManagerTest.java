package com.sfeir.business;

/**
 * Test  la classe Stock detail manager
 * @author kheireddine BOURAHLI
 */
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sfeir.persistence.entities.DetailStock;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StockDetailManagerTest {

	@Autowired
	private StockManagerManager stockManagerManager;

	@Test
	public void getAllStockDetailReturnsDataFromDatabase() throws Exception {
		List<DetailStock> detailStocks = stockManagerManager.getAllStockDetails().collect(Collectors.toList());
		assertFalse(detailStocks.isEmpty());
		assertEquals(5, detailStocks.size());
	}
}