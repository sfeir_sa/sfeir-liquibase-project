## Comment executer le projet?

## Recupérer le projet à partir du client git :

git clone https://bitbucket.org/sfeir_sa/sfeir-liquibase-project.git


1- Projet Spring boot:

- Accèder au projet spring boot:

         cd sfeir-liquibase-spring-boot-project

- Compiler le projet et executer le goal `package` de maven:

         mvn clean package

- Exécuter l'application spring boot:

        mvn spring-boot:run

- Aller vers URL suivant:

        http://localhost:8080/h2-console  
 
- Renseigner les paramètres de connexion de la base de données:

        `JDBC URL`: jdbc:h2:mem:database-sfeir

        `User Name`: sfeir  

        `Password`: sfeir

- Maintenant, Vous pouvez d'accéder à la base de données H2. 



2- Projet Spring boot:

- Accèder au projet maven:

        cd sfeir-liquibase-maven-project

- Exécuter la ligne de commande suivante:

        mvn liquibase:update
        